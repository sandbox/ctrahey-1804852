<?php
/**
 * @file views_slideshow.api.php
 * 
 * Copyright (C) 2012 Metal Toad Media
 * @author Chris Trahey <christrahey@gmail.com> www.christrahey.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file views_slideshows.api.php
 * Describes the API for generating blocks in the system which present views-backed slideshows.
 */

/**
 * Add a block to the system which can be configured with views.
 * @param $name string the Human-readable name of the block
 * @param $default_config array default config to use in hook_block when initing
 * @return string the machine name, use as $block_delta param to other API calls
 */
function views_slideshows_add_block($name, $default_config) {}

/**
 * Adds a view display to a block's slideshow
 * @param $block_delta string machine name of the block
 * @param $view string the machine name of the view
 * @param $display string the machine name of the display to include in the slideshow
 */
function views_slideshows_enable_display_for_block($block_delta, $view, $display) {}

/**
 * Sets the parameters to pass to the javascript object in-browser.
 * @param $block_delta string machine name of the block
 * @param $speed int The transition speed of slide changes, in milliseconds
 * @param $pause int The pause time between transitions, in milliseconds
 */
function views_slideshows_set_parameters($block_delta, $speed, $pause) {}

/**
 * Enables a view to be a provider of displays for slideshows.
 * This is essentially to de-clutter the block settings form
 * @param $view_name string machine name of view to specify as a provider for slideshows
 */
function views_slideshows_enable_view_for_use($view_name) {}
