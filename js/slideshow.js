/**
 * ViewsSlideshowAjaxLoader
 * Controls an instance of a views-backed slideshow.
 * @param block_id string, unique identifier for this slideshow on the page.
 * @param url array the source URLs to request pages from
 * @speed int transition duration in miliseconds
 * @pause int time to display each page between transitions
 */
function ViewsSlideshowAjaxLoader(block_id, urls, speed, pause) {
  this.first = 0;
  this.speed = speed;
  this.pause = pause;
  this.pageURLs = urls;
  this.block_id = block_id;
  var self = this;

  /**
   * sets the context container's height to the greatest view's height + 18px
   * Uses the pre-loaded views, so if a later page is taller, the rendered page may jump.
   */
  this.initHeight = function() {
    var maxHeight = 0;
    $('.view', self.context()).each(function() {
      var thisHeight = $(this).height();
      maxHeight =  (thisHeight > maxHeight) ? thisHeight : maxHeight;
    });
    self.context().height(maxHeight+18);
  };
  
  /**
   * Trigger the transition to the next page.
   * Hides and removes the current page and then triggers showing the next up.
   * Also triggers AJAX loading of one more page into the queue
   */
  this.nextImage = function() {
    $('.view:first', self.context()).animate({opacity:0}, self.speed).fadeOut('slow', function() {$(this).remove(); self.showFirst();});
    self.loadNextPage();
  };
  
  /**
   * Display the presumably-hidden page which is the first-child of context
   */
  this.showFirst = function() {
    var firstPage = $('.view:first', self.context());
    firstPage.fadeIn('slow').animate({opacity:1}, self.speed);
  };
  
  /**
   * retreive a (cached) jQuery object representing our container element.
   * Generally, this is the direct parent of the .view elements (the actual view output)
   */
  this.context = function() {
    if(!self.contextNode) {
      self.contextNode = $('#' + self.block_id + ' div.content').first();
      $('.view:not(:first)', self.contextNode).css('opacity', 0).hide();
    }
    return self.contextNode;
  };
  
  /**
   * Trigger Asynchronous loading of another page.
   * for now, this picks a view at random from our urls.
   * @todo implement various "strategies" for this, including:
   *       - Simple Paging
   *       - Weighted random (i.e. user picks more important URLs)
   *       - Advanced paging (eiher round-robin with page-advance, or exhause one url's pages then move on)
   */
  this.loadNextPage = function() {
    var randomIndex = Math.floor((Math.random() * 100) % self.pageURLs.length)
    $.getJSON(self.pageURLs[randomIndex], self.pageLoaded);
  };
  
  /**
   * AJAX Success callback for loading next view
   * Simply appends the display to our context
   * @param respons JSON expected to have {display:"<div class=view>..."}
   */
  this.pageLoaded = function(response) {
    if(response.display) {
      var thisPage = $(response.display);
      thisPage.hide();
      self.context().append(thisPage);
    }
  };
  
  // Bootstrap a few things
  this.contextNode = this.context();
  this.initHeight();
}
